#include <stream9/sd_event/event.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(event_)

    BOOST_AUTO_TEST_CASE(construct_)
    {
        ev::event e;
    }

    BOOST_AUTO_TEST_CASE(add_defer_1)
    {
        ev::event e;
        bool called = false;

        auto source = e.add_defer([&](auto) {
            e.exit(0);
            called = true;
            return 0;
        });

        e.loop();

        BOOST_TEST(called);
    }

    BOOST_AUTO_TEST_CASE(add_time_1)
    {
        using namespace std::literals;
        namespace C = std::chrono;

        ev::event e;
        bool called = false;

        auto now = C::system_clock::now();

        auto s = e.add_time(now + 1s, [&](auto, auto) {
            called = true;
            e.exit(0);
            return 0;
        });

        e.loop();

        BOOST_TEST(called);
    }

    BOOST_AUTO_TEST_CASE(add_time_relative_1)
    {
        using namespace std::literals;
        namespace C = std::chrono;

        ev::event e;
        bool called = false;

        auto s = e.add_time_relative(500ms, [&](auto, auto) {
            called = true;
            e.exit(0);
            return 0;
        });

        e.loop();

        BOOST_TEST(called);
    }

BOOST_AUTO_TEST_SUITE_END() // event_

} // namespace testing
