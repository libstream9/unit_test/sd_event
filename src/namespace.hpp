#ifndef STREAM9_SDBUS_TEST_SRC_SD_EVENT_NAMESPACE_HPP
#define STREAM9_SDBUS_TEST_SRC_SD_EVENT_NAMESPACE_HPP

namespace stream9::sd_event {}

namespace testing {

namespace ev { using namespace stream9::sd_event; }

} // namespace testing

#endif // STREAM9_SDBUS_TEST_SRC_SD_EVENT_NAMESPACE_HPP
